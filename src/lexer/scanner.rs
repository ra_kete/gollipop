use std::{iter::Peekable, str::CharIndices};
use unicode_xid::UnicodeXID;

#[derive(Debug, Clone, Copy, PartialEq)]
pub(super) enum Kind {
    Comment,
    Whitespace,
    Newline,

    Ident,
    Int,
    Float,
    Imag,
    Rune,
    String,
    RawString,

    Add,
    Sub,
    Mul,
    Quo,
    Rem,

    And,
    Or,
    Xor,
    Shl,
    Shr,
    AndNot,

    AddAssign,
    SubAssign,
    MulAssign,
    QuoAssign,
    RemAssign,
    AndAssign,
    OrAssign,
    XorAssign,
    ShlAssign,
    ShrAssign,
    AndNotAssign,

    LAnd,
    LOr,
    Arrow,
    Inc,
    Dec,

    Eq,
    NotEq,
    Less,
    Greater,
    LessEq,
    GreaterEq,
    Not,

    Assign,
    Define,
    Ellipsis,

    LParen,
    RParen,
    LBracket,
    RBracket,
    LBrace,
    RBrace,
    Comma,
    Period,
    Semicolon,
    Colon,

    Invalid,
}

fn is_whitespace(c: char) -> bool {
    c == ' ' || c == '\t' || c == '\r'
}

fn is_ident_start(c: char) -> bool {
    c >= 'a' && c <= 'z'
        || c >= 'A' && c <= 'Z'
        || c == '_'
        || c > '\x7f' && UnicodeXID::is_xid_start(c)
}

fn is_ident_cont(c: char) -> bool {
    c >= 'a' && c <= 'z'
        || c >= 'A' && c <= 'Z'
        || c >= '0' && c <= '9'
        || c == '_'
        || c > '\x7f' && UnicodeXID::is_xid_continue(c)
}

fn is_decimal_digit(c: char) -> bool {
    c >= '0' && c <= '9'
}

fn is_hex_digit(c: char) -> bool {
    c >= '0' && c <= '9' || c >= 'a' && c <= 'z' || c >= 'A' && c <= 'Z'
}

pub(super) struct Scanner<'i> {
    indices: Peekable<CharIndices<'i>>,
    current: Option<char>,
    pos: usize,
}

impl<'i> Scanner<'i> {
    pub(super) fn new(input: &'i str) -> Self {
        let mut indices = input.char_indices().peekable();
        let current = indices.next().map(|(_, c)| c);
        Self {
            indices,
            current,
            pos: 0,
        }
    }

    fn bump(&mut self) {
        if let Some((pos, c)) = self.indices.next() {
            self.current = Some(c);
            self.pos = pos;
        } else {
            if let Some(c) = self.current {
                self.pos += c.len_utf8();
            }
            self.current = None;
        }
    }

    fn peek(&mut self) -> Option<char> {
        self.indices.peek().map(|(_, c)| *c)
    }

    fn bump_while<F>(&mut self, pred: F)
    where
        F: Fn(char) -> bool,
    {
        loop {
            match self.current {
                Some(c) if pred(c) => self.bump(),
                _ => break,
            }
        }
    }

    fn bump_and_scan(&mut self, kind: Kind) -> Kind {
        self.bump();
        kind
    }

    fn scan(&mut self, c: char) -> Kind {
        if is_whitespace(c) {
            return self.scan_whitespace();
        }
        if is_ident_start(c) {
            return self.scan_ident();
        }

        match c {
            '\n' => Kind::Newline,
            '0' => match self.current {
                Some('x') | Some('X') => self.scan_hex_int(),
                _ => self.scan_number(),
            },
            '1'..='9' => self.scan_number(),
            '.' => match (self.current, self.peek()) {
                (Some('.'), Some('.')) => self.scan_ellipsis(),
                (Some(c), _) if is_decimal_digit(c) => self.scan_float(),
                _ => Kind::Period,
            },
            '\'' => self.scan_rune(),
            '"' => self.scan_string(),
            '`' => self.scan_raw_string(),
            '+' => match self.current {
                Some('=') => self.bump_and_scan(Kind::AddAssign),
                Some('+') => self.bump_and_scan(Kind::Inc),
                _ => Kind::Add,
            },
            '-' => match self.current {
                Some('=') => self.bump_and_scan(Kind::SubAssign),
                Some('-') => self.bump_and_scan(Kind::Dec),
                _ => Kind::Sub,
            },
            '*' => match self.current {
                Some('=') => self.bump_and_scan(Kind::MulAssign),
                _ => Kind::Mul,
            },
            '/' => match self.current {
                Some('/') => self.scan_line_comment(),
                Some('*') => self.scan_general_comment(),
                Some('=') => self.bump_and_scan(Kind::QuoAssign),
                _ => Kind::Quo,
            },
            '%' => match self.current {
                Some('=') => self.bump_and_scan(Kind::RemAssign),
                _ => Kind::Rem,
            },
            '&' => match self.current {
                Some('^') => {
                    self.bump();
                    match self.current {
                        Some('=') => self.bump_and_scan(Kind::AndNotAssign),
                        _ => Kind::AndNot,
                    }
                }
                Some('=') => self.bump_and_scan(Kind::AndAssign),
                Some('&') => self.bump_and_scan(Kind::LAnd),
                _ => Kind::And,
            },
            '|' => match self.current {
                Some('=') => self.bump_and_scan(Kind::OrAssign),
                Some('|') => self.bump_and_scan(Kind::LOr),
                _ => Kind::Or,
            },
            '^' => match self.current {
                Some('=') => self.bump_and_scan(Kind::XorAssign),
                _ => Kind::Xor,
            },
            '<' => match self.current {
                Some('<') => {
                    self.bump();
                    match self.current {
                        Some('=') => self.bump_and_scan(Kind::ShlAssign),
                        _ => Kind::Shl,
                    }
                }
                Some('-') => self.bump_and_scan(Kind::Arrow),
                Some('=') => self.bump_and_scan(Kind::LessEq),
                _ => Kind::Less,
            },
            '>' => match self.current {
                Some('>') => {
                    self.bump();
                    match self.current {
                        Some('=') => self.bump_and_scan(Kind::ShrAssign),
                        _ => Kind::Shr,
                    }
                }
                Some('=') => self.bump_and_scan(Kind::GreaterEq),
                _ => Kind::Greater,
            },
            '=' => match self.current {
                Some('=') => self.bump_and_scan(Kind::Eq),
                _ => Kind::Assign,
            },
            '!' => match self.current {
                Some('=') => self.bump_and_scan(Kind::NotEq),
                _ => Kind::Not,
            },
            ':' => match self.current {
                Some('=') => self.bump_and_scan(Kind::Define),
                _ => Kind::Colon,
            },
            '(' => Kind::LParen,
            '[' => Kind::LBracket,
            '{' => Kind::LBrace,
            ',' => Kind::Comma,
            ')' => Kind::RParen,
            ']' => Kind::RBracket,
            '}' => Kind::RBrace,
            ';' => Kind::Semicolon,
            _ => Kind::Invalid,
        }
    }

    fn scan_whitespace(&mut self) -> Kind {
        self.bump_while(is_whitespace);
        Kind::Whitespace
    }

    fn scan_line_comment(&mut self) -> Kind {
        // we are here: '//...'
        //                ^

        self.bump_while(|c| c != '\n');
        Kind::Comment
    }

    fn scan_general_comment(&mut self) -> Kind {
        // we are here: '/*...'
        //                ^

        let mut newline = false;
        loop {
            self.bump();
            match self.current {
                Some('\n') => newline = true,
                Some('*') => {
                    if self.peek() == Some('/') {
                        break;
                    }
                }
                Some(_) => (),
                None => return Kind::Invalid,
            }
        }

        // we are here: '/*...*/'
        //                    ^

        self.bump();

        if newline {
            // force a Newline to be scanned next
            self.current = Some('\n');
        } else {
            self.bump();
        }

        Kind::Comment
    }

    fn scan_ident(&mut self) -> Kind {
        self.bump_while(is_ident_cont);
        Kind::Ident
    }

    fn scan_number(&mut self) -> Kind {
        self.bump_while(is_decimal_digit);

        match self.current {
            Some('.') | Some('e') | Some('E') => self.scan_float(),
            Some('i') => self.bump_and_scan(Kind::Imag),
            _ => Kind::Int,
        }
    }

    fn scan_hex_int(&mut self) -> Kind {
        // we are here: '0x...'
        //                ^

        self.bump_while(is_hex_digit);
        Kind::Int
    }

    fn scan_float(&mut self) -> Kind {
        // we are after the integer part

        match self.current {
            Some('e') | Some('E') => return self.scan_float_exp(),
            Some('.') => self.bump(),
            _ => (),
        }

        self.bump_while(is_decimal_digit);

        match self.current {
            Some('e') | Some('E') => self.scan_float_exp(),
            Some('i') => self.bump_and_scan(Kind::Imag),
            _ => Kind::Float,
        }
    }

    fn scan_float_exp(&mut self) -> Kind {
        // we are here: '12.3e...'
        //                   ^

        self.bump();

        match self.current {
            Some('+') | Some('-') => self.bump(),
            _ => (),
        }

        match self.current {
            Some(c) if is_decimal_digit(c) => (),
            _ => return Kind::Invalid,
        }

        self.bump_while(is_decimal_digit);

        match self.current {
            Some('i') => self.bump_and_scan(Kind::Imag),
            _ => Kind::Float,
        }
    }

    fn scan_rune(&mut self) -> Kind {
        loop {
            match self.current {
                Some('\\') => self.bump(),
                Some('\'') => break,
                Some('\n') | None => return Kind::Invalid,
                _ => (),
            }
            self.bump();
        }

        self.bump();
        Kind::Rune
    }

    fn scan_string(&mut self) -> Kind {
        loop {
            match self.current {
                Some('\\') => self.bump(),
                Some('"') => break,
                Some('\n') | None => return Kind::Invalid,
                _ => (),
            }
            self.bump();
        }

        self.bump();
        Kind::String
    }

    fn scan_raw_string(&mut self) -> Kind {
        self.bump_while(|c| c != '`');

        if self.current.is_none() {
            return Kind::Invalid;
        }

        self.bump();
        Kind::RawString
    }

    fn scan_ellipsis(&mut self) -> Kind {
        // we are here: '...'
        //                ^

        self.bump();
        self.bump();
        Kind::Ellipsis
    }
}

impl<'i> Iterator for Scanner<'i> {
    type Item = (Kind, usize);

    fn next(&mut self) -> Option<Self::Item> {
        let c = self.current?;
        let start = self.pos;

        self.bump();
        let kind = self.scan(c);
        let len = self.pos - start;

        Some((kind, len))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn scan(input: &str) -> Vec<(Kind, usize)> {
        let scanner = Scanner::new(input);
        scanner.collect()
    }

    fn scan_kinds(input: &str) -> Vec<Kind> {
        scan(input).into_iter().map(|(k, _)| k).collect()
    }

    macro_rules! test_scan {
        ( $( $name:ident: $input:expr => $result:expr, )* ) => {
            $(
                #[test]
                fn $name() {
                    assert_eq!(scan($input)[0], $result);
                }
            )*
        };
    }

    test_scan! {
        whitespace: " \t\r" => (Kind::Whitespace, 3),
        newline:    "\n\n"  => (Kind::Newline, 1),

        comment_1: "/* a comment */" => (Kind::Comment, 15),
        comment_2: "// a comment \n" => (Kind::Comment, 13),
        comment_3: "/*\r*/"          => (Kind::Comment, 5),
        comment_4: "/**\r/*/"        => (Kind::Comment, 7),
        comment_5: "/**\r\r/*/"      => (Kind::Comment, 8),
        comment_6: "//\r\n"          => (Kind::Comment, 3),

        ident_1: "foobar"  => (Kind::Ident, 6),
        ident_2: "a۰۱۸"    => (Kind::Ident, 7),
        ident_3: "foo६४"   => (Kind::Ident, 9),
        ident_4: "bar９８７６" => (Kind::Ident, 15),
        ident_5: "ŝ"       => (Kind::Ident, 2),
        ident_6: "ŝfoo"    => (Kind::Ident, 5),

        int_1: "0"                     => (Kind::Int, 1),
        int_2: "1"                     => (Kind::Int, 1),
        int_3: "123456789012345678890" => (Kind::Int, 21),
        int_4: "01234567"              => (Kind::Int, 8),
        int_5: "0xcafebabe"            => (Kind::Int, 10),
        int_6: "0XCAFEBABE"            => (Kind::Int, 10),

        float_1: "0."            => (Kind::Float, 2),
        float_2: ".0"            => (Kind::Float, 2),
        float_3: "3.14159265"    => (Kind::Float, 10),
        float_4: "1e0"           => (Kind::Float, 3),
        float_5: "1e+100"        => (Kind::Float, 6),
        float_6: "1E-100"        => (Kind::Float, 6),
        float_7: "2.71828e-1000" => (Kind::Float, 13),

        imag_1:  "0i"                     => (Kind::Imag, 2),
        imag_2:  "1i"                     => (Kind::Imag, 2),
        imag_3:  "012345678901234567889i" => (Kind::Imag, 22),
        imag_4:  "123456789012345678890i" => (Kind::Imag, 22),
        imag_5:  "0.i"                    => (Kind::Imag, 3),
        imag_6:  ".0i"                    => (Kind::Imag, 3),
        imag_7:  "3.14159265i"            => (Kind::Imag, 11),
        imag_8:  "1e0i"                   => (Kind::Imag, 4),
        imag_9:  "1e+100i"                => (Kind::Imag, 7),
        imag_10: "1e-100i"                => (Kind::Imag, 7),
        imag_11: "2.71828e-1000i"         => (Kind::Imag, 14),

        rune_1: r"'a'"          => (Kind::Rune, 3),
        rune_2: r"'\000'"       => (Kind::Rune, 6),
        rune_3: r"'\xFF'"       => (Kind::Rune, 6),
        rune_4: r"'\uff16'"     => (Kind::Rune, 8),
        rune_5: r"'\U0000ff16'" => (Kind::Rune, 12),
        rune_6: r"'\''"         => (Kind::Rune, 4),

        string_1: r#""foobar""#     => (Kind::String, 8),
        string_2: r#""foo\nbar""#   => (Kind::String, 10),
        string_3: r#""foo\"bar""#   => (Kind::String, 10),
        string_4: r#""\000""#       => (Kind::String, 6),
        string_5: r#""\xFF""#       => (Kind::String, 6),
        string_6: r#""\uff16""#     => (Kind::String, 8),
        string_7: r#""\U0000ff16""# => (Kind::String, 12),

        raw_string_1: "`foobar`"     => (Kind::RawString, 8),
        raw_string_2: "`foo
                        bar`"        => (Kind::RawString, 33),
        raw_string_3: "`\r`"         => (Kind::RawString, 3),
        raw_string_4: "`foo\r\nbar`" => (Kind::RawString, 10),

        add: "+" => (Kind::Add, 1),
        sub: "-" => (Kind::Sub, 1),
        mul: "*" => (Kind::Mul, 1),
        quo: "/" => (Kind::Quo, 1),
        rem: "%" => (Kind::Rem, 1),

        and:     "&"  => (Kind::And, 1),
        or:      "|"  => (Kind::Or, 1),
        xor:     "^"  => (Kind::Xor, 1),
        shl:     "<<" => (Kind::Shl, 2),
        shr:     ">>" => (Kind::Shr, 2),
        and_not: "&^" => (Kind::AndNot, 2),

        add_assign:     "+="  => (Kind::AddAssign, 2),
        sub_assign:     "-="  => (Kind::SubAssign, 2),
        mul_assign:     "*="  => (Kind::MulAssign, 2),
        quo_assign:     "/="  => (Kind::QuoAssign, 2),
        rem_assign:     "%="  => (Kind::RemAssign, 2),
        and_assign:     "&="  => (Kind::AndAssign, 2),
        or_assign:      "|="  => (Kind::OrAssign, 2),
        xor_assign:     "^="  => (Kind::XorAssign, 2),
        shl_assign:     "<<=" => (Kind::ShlAssign, 3),
        shr_assign:     ">>=" => (Kind::ShrAssign, 3),
        and_not_assign: "&^=" => (Kind::AndNotAssign, 3),

        land:  "&&" => (Kind::LAnd, 2),
        lor:   "||" => (Kind::LOr, 2),
        arrow: "<-" => (Kind::Arrow, 2),
        inc:   "++" => (Kind::Inc, 2),
        dec:   "--" => (Kind::Dec, 2),

        eq:         "==" => (Kind::Eq, 2),
        not_eq:     "!=" => (Kind::NotEq, 2),
        less:       "<"  => (Kind::Less, 1),
        greater:    ">"  => (Kind::Greater, 1),
        less_eq:    "<=" => (Kind::LessEq, 2),
        greater_eq: ">=" => (Kind::GreaterEq, 2),
        not:        "!"  => (Kind::Not, 1),

        assign: "="     => (Kind::Assign, 1),
        define: ":="    => (Kind::Define, 2),
        ellipsis: "..." => (Kind::Ellipsis, 3),

        lparen:    "(" => (Kind::LParen, 1),
        rparen:    ")" => (Kind::RParen, 1),
        lbracket:  "[" => (Kind::LBracket, 1),
        rbracket:  "]" => (Kind::RBracket, 1),
        lbrace:    "{" => (Kind::LBrace, 1),
        rbrace:    "}" => (Kind::RBrace, 1),
        comma:     "," => (Kind::Comma, 1),
        period:    "." => (Kind::Period, 1),
        semicolon: ";" => (Kind::Semicolon, 1),
        colon:     ":" => (Kind::Colon, 1),
    }

    #[test]
    fn newline_follows_line_comment() {
        let kinds = scan_kinds("// a comment\n");
        assert_eq!(kinds, [Kind::Comment, Kind::Newline])
    }

    #[test]
    fn newline_follows_multiline_comment() {
        let kinds = scan_kinds("/* a comment\n on two lines*/");
        assert_eq!(kinds, [Kind::Comment, Kind::Newline])
    }

    #[test]
    fn newline_does_not_follow_singleline_comment() {
        let kinds = scan_kinds("/* a comment on one line*/");
        assert_eq!(kinds, [Kind::Comment])
    }
}
